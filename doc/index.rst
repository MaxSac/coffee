Welcome to CoffeePy's documentation!
====================================

.. toctree::
   :maxdepth: 1

   installation/index
   documentation/index
   examples/index
   tutorials/index
   contribution/index


What's this all about?
----------------------

Motivated by outdated coffee lists,
irregular cleaning and deposits,
the idea of the CoffeePy was born.
By providing information on coffee consumption,
billing by using RFID chips
and the expandability of the range of functions,
the payment of the next coffee should become an experience.
Addressed are all people who are interested in data analysis,
extend their python knowledge,
finish a construction kit
or just want an automated coffee list.


Why should I use the CoffeePy?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Loans can lead to great distress,
if something does not go as usual.
Avoid this risk by using the transparent solution
and save yourself a lot of additional accounting work.
From now on you can run the cash register with maximum transparency
and minimum effort.

Cleaning plans help to keep the coffee machine clean
and thus ensure safe and regulated operation.
Automated requests for cleaning can ensure a fair
and consistent division of tasks.

Data analysis is more fun,
when you discover connections you would never have imagined.
Can you imagine what coffee consumption correlates with?
Check it out, improve your analysis skills
and find exciting correlations.


Features make a good gadget.
Improve your python skills,
add some and share them with others.
Create the coffee list according to your needs and wishes,
or simply use the provided features.


How could I get one?
^^^^^^^^^^^^^^^^^^^^

The project is concept as a cheap self made project,
for less cost than ten packs of coffee.
All you need is a Pi, Display, RFID-Reader
and a little bit of time.
For administration task there is also a temporary internet
connection required.
More information of the required parts are listed in the
:doc:`installation/components` section.


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
