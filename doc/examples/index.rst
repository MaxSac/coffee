Examples
========

Get a minimal working setup
---------------------------

You first have to set up the database with at least a single product::

    coffeepy init
    coffeepy add product

and follow the steps on the command line.

After this, you can already start the coffeepy machine::

    coffeepy start

Users now can identify themselves and drink the product(s).
The RFID will be saved into the database, but without information about the
users.

We presume the new users will come to you, as it is displayed.
Get the id of this new user::

    coffeepy show user -l/--last-added

and change the information of this user::

    coffeepy change user

(follow the displayed steps, you got the id in the step before).


Add credit to existing user
---------------------------

You have to pay for the coffee beans, and want to distribute the financial load
to the consumers.
We will work with a pre/post paid service based on trust.

You can update the credit of an existing user (after they paid you)::

    coffeepy add credit


* Readout current consumption
