Wiring
======

To ensure communication between the Raspberry Pi
and the components,
the General Purpose Input Output (GPIO) channels can be used.
The different pins are numberd and assigned with different channels and
functionalities.
A short overview about the GPIO Pinout and numbering can be found `here
<https://pinout.xyz/>`_.
It's advisable to connect the Pi and the components with wire cables (socket-socket/-jack jumper),
because this way ensures a simple modification and extensionability.

The numbers in the following table represent the physical GPIO pin numbering wiring scheme.
The Inky connections are kept as if it was stuck right on the Pins.

+--------------+------+------+
| Raspberry Pi | Inky | RFID |
| GPIO         |      |      |
+==============+======+======+
| 1            |    1 |      |
+--------------+------+------+
| 2            |    2 |      |
+--------------+------+------+
| 6            |    6 |      |
+--------------+------+------+
| 11           |   11 |      |
+--------------+------+------+
| 12           |      |  SDA |
+--------------+------+------+
| 13           |   13 |      |
+--------------+------+------+
| 15           |   15 |      |
+--------------+------+------+
| 17           |      | 3.3V |
+--------------+------+------+
| 18           |      |  IRQ |
+--------------+------+------+
| 19           |   19 |      |
+--------------+------+------+
| 20           |      |  GND |
+--------------+------+------+
| 22           |      |  RST |
+--------------+------+------+
| 23           |   23 |      |
+--------------+------+------+
| 24           |   24 |      |
+--------------+------+------+
| 35           |      | MISO |
+--------------+------+------+
| 38           |      | MOSI |
+--------------+------+------+
| 40           |      |  SCK |
+--------------+------+------+

Because the standard settings of the components use the same pins
they have to be reassigned by software.
Be sure to follow the steps in the software instructions to avoid errors
that are difficult to trace back.
