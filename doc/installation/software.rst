Software
========

If this is the first contact with the Rapberry Pi,
an operating system must first be installed.
An easy start can be done with the noobs_ installer
In general, no special Operating System (OS) is required.
Choose the one you like best and meet the hardware requirements.

.. image:: images/RPi.png
    :scale: 3%
    :align: center

The project was realized in Python as far as possible.
If you don't already have a Python installation contrary the expectations on your raspberry,
`here <https://www.python.org>`_ you can find instructions,
how to install it on an operating system of your choice.

The core library is mainly based on three libraries.
To manage the stored data in an SQL database, sqlalchemy_ is used as an object
relation mapper. Expect the two libraries which provide communication with the
sensors, it is more or less written in pure python.

.. _noobs: https://www.raspberrypi.org/downloads/noobs/
.. _Python: https://www.python.org
.. _sqlalchemy: https://www.sqlalchemy.org
.. _mfrc522: https://github.com/pimylifeup/MFRC522-python
.. _inky: https://github.com/pimoroni/inky


CoffeePy
--------

There are two options to install the neccessary software.
The easiest way is to use the python package indexer PyPi.
Simply Install the latest release from pypi::

    pip install coffeePy

and that's it.

Otherwise, if you are interested in the most up-to-date development version,
or are interested in developing yourself,
clone the repository from GitLab::

    git clone https://gitlab.com/MaxSac/coffee.git
    cd coffee
    pip install -e .

If there are install issues with numpy, refer to the `numpy docs`_.

.. _numpy docs: https://numpy.org/devdocs/user/troubleshooting-importerror.html#raspberry-pi


Wiring Setup
------------

The way the Raspberry Pi interacts with its hardware GPIO pins must be altered
a little bit.
We need to enable the SPI1 interface, add to the end of `/boot/config.txt`::

    dtoverlay=spi1-1cs


Final Steps
-----------

We need to make sure, that the software starts on Raspberry Pi startup,
otherwise this whole procedure could add more work to you than doing everything
manually.

Start your crontab::

    crontab -e

And append to the end of the file::

    @restart coffepy-start

Then, save and exit.

Everything is set up, congratulations.
Now, place your new coffeepi next to your coffee machine and power it up.
For the now coming administrative tasks, refer to
:doc:`/documentation/useability/index`.
