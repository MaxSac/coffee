Installation
============

.. toctree::
    :maxdepth: 1

    components
    wiring
    software
    housing

The installation process is very simple.
First, obtain the required parts listed in the :doc:`components` section.
In principle with some modified code,
the project can also be implemented with components
that you may still have at home,
or that are currently cheaper or more functional.

Reduce the waiting time for the parts by creating a caseing for your components.
You can find the external dimesions of the components
and a construction proposal in the :doc:`housing` section.
Once your components have arrived,
wire them as described in the :doc:`wiring` section
and install the software listed in the :doc:`software` section.

Now your coffee station should be ready for operation.
Place the coffeepy clearly visible near the coffee machine.
Your work colleagues will surely notice it during their next coffee break
and soon a lot of work will be done for you.
Have fun modifying and using it.
Useful tips for extending the functionality can be found
in the tutorial section.
