Components
==========

The workhorse of the coffee machine is a `Raspberry Pi`_.
If you can spare the dime, go for the `latest model`_,
although it is not needed.

Depending on your kitchen situation you may need to get one with inbuilt wifi
or a separate wifi dongle.

The second most important part is a RFID_ Reader.
It is used to carry out user identification and thus the accounting.
We use the AZDelivery RFID Kit RC522_ (amazon.de link).

To give visual feedback to the people getting coffee, a display
.. and later LEDs
is used.
Due to constraints of space and money, we opted for the `Inky wHAT`_,
which is a black and white `E Ink`_ display.
It fits just nicely directly above the Raspberry Pi, and has roughly the same
size.

In total **17** jumper wires are needed to :doc:`connect <wiring>` all the components. Of these, 8 are socket-socket and 9 are socket-jack.

To hide the bare electronics and make them somewhat spill-proof, a case is
needed.
We will built one ourselves using a 3D printer, see the :doc:`corresponding
section <housing>` of our manual.


.. links

.. _Raspberry Pi:  https://www.raspberrypi.org
.. _latest model:  https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
.. _RFID:  https://en.wikipedia.org/wiki/Radio-frequency_identification>
.. _RC522: https://www.amazon.de/gp/product/B01M28JAAZ/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1
.. _Inky wHAT: https://shop.pimoroni.com/products/inky-what
.. _E Ink: https://en.wikipedia.org/wiki/E_Ink

Cost
----

The following table will give an estimate of the costs.

+-----------------+---------+
|Part             |Price / €|
+=================+=========+
|Raspberry Pi     |       35|
+-----------------+---------+
|SD-Card > 2GB    |        5|
+-----------------+---------+
|Power Supply     |        5|
+-----------------+---------+
|RFID Scanner     |        5|
+-----------------+---------+
|Inky wHAT display|       45|
+-----------------+---------+
|Total            |       95|
+-----------------+---------+
