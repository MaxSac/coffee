# CoffeePi

To build the documentation (sphinx must be installed) run:
```bash
cd doc
make html
cd _build/html
python3 -m http.server
```
and point your browser to `localhost:8000` to read it.
