from setuptools import setup, find_packages

setup(
    name='CoffeePy',
    authors=['Sackel, Maximilan', 'Biederbeck, Noah'],
    version='0.1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'coffeepy=CoffeePy.admin_operations.management:main',
        ],
    },
)
