import logging
from logging.handlers import RotatingFileHandler


class CoffeeLogger:
    def __init__(
        self, package_name="CoffeePi", log_level="DEBUG", log_file="coffepi.log"
    ):
        """
        Logging module to listen on all modules with different log levels.

        It is a wrapper class to generate a logger to listen to all modules.
        There will be written all logs higher than DEBUG to a file.
        Everything higher than DEBUG will be printed in shell in simplified
        presentation.

        For further information look at
        `logging <https://docs.python.org/3/library/logging.html>`_.
        documentation.

        Parameters:
            package_name(string):   Name of the '?url?' logger is listening
            log_level(string):      main threshold of logger
            log_file(string):       Name of the log file
        Returns:
            None
        """
        self.logger = logging.getLogger(package_name)
        self.logger.setLevel(log_level)
        # CRITICAL, ERROR, WARNING, INFO, DEBUG

        self.fh = RotatingFileHandler(
            log_file, mode="a", maxBytes=4096, backupCount=1
        )
        self.fh.setLevel(logging.DEBUG)
        file_formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        self.fh.setFormatter(file_formatter)
        self.logger.addHandler(self.fh)

        self.sh = logging.StreamHandler()
        self.sh.setLevel(logging.INFO)
        shell_formatter = logging.Formatter("%(asctime)s - %(message)s")
        self.sh.setFormatter(shell_formatter)
        self.logger.addHandler(self.sh)

    def get_shellHandler(self):
        return self.sh

    def get_fileHandler(self):
        return self.fh
