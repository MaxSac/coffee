import logging
from CoffeeLogger import CoffeeLogger
from pathlib import Path

# create logger
module_logger = logging.getLogger("CoffeePi.test_coffeLogging")


class Auxiliary:
    def __init__(self):
        self.logger = logging.getLogger("CoffeePi.Auxiliary")
        self.logger.debug("creating an instance of Auxiliary")

    def do_something(self):
        self.logger.info("doing something")
        a = 1 + 1
        self.logger.info("done doing something")


def some_function():
    module_logger.info('received a call to "some_function"')


TEST_FILE = "test.log"
if Path(TEST_FILE).is_file():
    Path(TEST_FILE).unlink()


class TestLogger:
    module_logger = logging.getLogger("CoffeePi.test_coffeLogging.TestLogger")

    def test_createLogFile(self):
        """Check if logfile is correctly created."""
        log = CoffeeLogger(log_file=TEST_FILE)
        module_logger.info("createing Logfile.")
        assert Path(TEST_FILE).is_file()

    def test_checkLogLevel(self):
        """Check if only logs that are higher than threshold are written"""
        CoffeeLogger(log_level="INFO", log_file=TEST_FILE)
        module_logger.info("foo")
        module_logger.debug("bar")
        foo_counter, bar_counter = 0, 0
        log_file = open(TEST_FILE, "r")
        for l in log_file.readlines():
            words = l.split()
            foo_counter += "foo" in words
            bar_counter += "bar" in words
        assert foo_counter > 0
        assert bar_counter == 0

    def test_filesize(self):
        """
        Check if filesize is smaller/equal the 4096 bytes and backup file is
        created.
        """
        CoffeeLogger(log_file=TEST_FILE)
        module_logger.info("a" * 2048 + "h")
        module_logger.info("a" * 2048 + "h")
        assert Path(TEST_FILE).stat().st_size <= 4096
        assert Path(TEST_FILE + ".1").is_file()

    def test_moduleCall(self):
        """
        Check if module logger created right name and log the message.
        """
        CoffeeLogger(log_file=TEST_FILE)
        Auxiliary().do_something()
        log_file = open(TEST_FILE, "r")
        lines = log_file.readlines()
        a, b = 0, 0
        for l in lines:
            a += "doing something" in l and "CoffeePi.Auxiliary" in l
            b += "done doing something" in l and "CoffeePi.Auxiliary" in l
        assert a > 0
        assert b > 0
