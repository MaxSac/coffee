import smtplib
import yaml
import logging
from email.message import EmailMessage

module_logger = logging.getLogger("CoffeePi.coffeemail")


class CoffeeMail:
    module_logger = logging.getLogger("CoffeePi.coffeemail")

    def __init__(self, config_path="email_config.yml"):
        with open(config_path) as f:
            email_config = yaml.load(f, Loader=yaml.FullLoader)
        self.mail_admin = email_config["mail_admin"]
        self.mail_password = email_config["mail_password"]
        self.logger = logging.getLogger("CoffeePi.coffeemail.CoffeeMail")

    def login(self):
        self.logger.debug(f"login {self.mail_admin} with password {self.password}.")
        self.server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        self.server.ehlo()
        self.server.login(self.mail_admin, self.mail_password)

    def send(self, recipient, subject, content):
        self.logger.info(f"Send mail to {recipient} with subject {subject}.")
        msg = EmailMessage()
        msg["To"] = recipient
        msg["From"] = "CoffeePi"
        msg["Subject"] = subject
        msg.set_content(content)
        self.server.send_message(msg)
        self.logger.debug(f"Sending mail to {recipient} successfully.")


def main():
    mail = CoffeeMail()
    mail.login()
    mail.send(
        "max.mustermann@udo.edu",
        "Hier schreibt die Kaffeemaschine",
        "Hallo Herr Biederbeck.\nIhre Kaffeemaschine freut sich auf eine Gute zusammenarbeit. Lassen Sie doch mal wieder was von sich hoeren. Ich hoffe ich konnte ihnen die langweiligen Nachmittagsstunden etwas ertraeglicher machen.\nMfG CoffeePi",
    )


if __name__ == "__main__":
    main()
