from urllib.request import urlopen


HTTP_SUCCESS = 200


class CoffeeTelegram:
    def __init__(self, chat_id, bot_token):
        self.url = (
            "https://api.telegram.org/"
            + f"bot{bot_token}/"
            + f"sendMessage?chat_id={chat_id}"
            + "&text="
        )

    def parse_message(self, message):
        return message.replace(" ", "%20").replace("\n", "%0A")

    def send(self, message):
        msg = self.parse_message(message)
        response = urlopen(self.url + msg)
        return response.code == HTTP_SUCCESS
