"""Unify communication with users."""


class CoffeeCommunication:
    def __init__(self, channels: list):
        self.channels = channels

    # proposed
    def send(self, msg):
        for cnl in self.channels:
            cnl.send(msg)
