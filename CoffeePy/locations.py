from pathlib import Path

config_dir = Path("~/.config/coffeepy/").expanduser()
data_dir = Path("~/.local/share/coffeepy/").expanduser()

config_dir.mkdir(parents=True, exist_ok=True)
data_dir.mkdir(parents=True, exist_ok=True)

config_file = config_dir / "coffeepy.yaml"
database = data_dir / "coffee.db"
