from CoffeePy.rfid import CoffeeScanner


def test_mocking():
    """This test should run on every machine, including Raspberry Pis."""
    rfid = CoffeeScanner()
    user_id = rfid._mock_read(user_id="1337")
    assert user_id == "1337"
