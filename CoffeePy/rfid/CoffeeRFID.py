"""RFID Scanner."""
import logging


module_logger = logging.getLogger("CoffeePi.CoffeeRFID")


try:
    import RPi.GPIO as GPIO
    from mfrc522 import SimpleMFRC522
    GPIO.setwarnings(False)
    RASPBERRY_PI = True

except (ModuleNotFoundError, RuntimeError):
    module_logger.error("Could not import GPIO packages.")
    module_logger.warning("Assume not running on Raspberry Pi. Continue.")

    from CoffeePy.rfid.MockRFID import MockRFID
    RASPBERRY_PI = False


class CoffeeRFID:

    """Very Simple RFID Scanner.

    Just create an instance and call it.

    """

    def __init__(self):
        self.logger = logging.getLogger("CoffeePi.coffeerfid.coffeerfid")
        if RASPBERRY_PI:
            self.reader = SimpleMFRC522(bus=1, device=0, pin_mode=11, pin_rst=25)
        else:
            self.reader = MockRFID()

    def __call__(self):
        """
        Returns:
            int
        """
        user_id = self.reader.read_id()
        self.logger.debug(f"Read user_id {user_id}")
        return str(user_id)

    def _mock_read(self, user_id="42"):
        """For testing and implementing without a Raspberry Pi.

        Parameters:
            user_id (str)

        Returns:
            int
        """
        self.logger.debug(f"Mocking reading of user_id {user_id}")
        return str(user_id)


if __name__ == "__main__":
    rfid = CoffeeRFID()
    print(rfid())
