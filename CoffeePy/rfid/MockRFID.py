"""Mock RFID Scanner."""
import logging

module_logger = logging.getLogger("CoffeePy.MockRFID")


class MockRFID:
    def __init__(self):
        pass

    def read_id(self):
        return "42"
