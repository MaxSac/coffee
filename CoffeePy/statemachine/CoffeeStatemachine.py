import logging
from datetime import datetime

from CoffeePy.misc import create_qr_image

from CoffeePy.database.CoffeeSQL import Consumption, Credit, Product, SQL_Handler, User
from CoffeePy.display import CoffeeDisplay as display
from CoffeePy.rfid import CoffeeRFID as scanner

module_logger = logging.getLogger("CoffeePi.coffeeStatemachine_module")


def now():
    """Get date/time for 'now'."""
    return datetime.now().strftime("%d. %B %Y %H:%M")


class State:
    def run(self, data=None):
        assert 0, "run not implemented"

    def next(self, data=None):
        assert 0, "next not implemented"


class StateMachine:
    def __init__(self, initialState, data=None):
        self.currentState = initialState
        self.data = data

    def run(self):
        self.data = self.currentState.run(self.data)
        self.currentState = self.currentState.next(self.data)


class CoffeeMachine(StateMachine):
    def __init__(self):
        StateMachine.__init__(self, CoffeeMachine.wait)


class Wait(State):
    def __init__(self, display):
        self.disp = display

    def run(self, data):
        date = now()
        info = (
            "Hello.\n"
            + "Put your unicard on the rfid chip reader.\n"
            + "For questions and suggestions contact the admins (Noah / Maximilian).\n"
            + date
        )
        self.disp.show(info)

    def next(self, data):
        return CoffeeMachine.identify_user


class IdentifyUser(State):
    def __init__(self, scanner, sql):
        self.scanner = scanner
        self.sql_handler = sql

    def run(self, data):
        rfid_id = self.scanner()
        return {
            "user": self.sql_handler.query(User)
            .filter(User.rfid_id == rfid_id)
            .first(),
            "rfid": rfid_id,
        }

    def next(self, data):
        if not data["user"]:
            return CoffeeMachine.welcome_new_user
        if not data["user"].first_name:
            return CoffeeMachine.welcome_not_authenticated
        return CoffeeMachine.welcome_existing_user


class Welcome_existing_user(State):
    def __init__(self, disp):
        self.disp = disp

    def run(self, data):
        self.disp.show(f"Welcome {data['user'].first_name}.")
        return data

    def next(self, data):
        return CoffeeMachine.checkout_coffee


class Welcome_not_authenticated(State):
    def __init__(self, disp):
        self.disp = disp

    def run(self, data):
        img = create_qr_image(rfid=data["rfid"])
        self.disp.show_image(img)
        return data

    def next(self, data):
        return CoffeeMachine.wait


class Welcome_new_user(State):
    """Noch nicht gesehene RFID.

    Prev
    ----
    Identify User

    Next
    ----
    Wait
    """

    def __init__(self, display, sql):
        self.disp = display
        self.sql_handler = sql

    def run(self, data):
        self.disp.show(
            f"Welcome fellow coffee-addict!\n Before you can use the coffee till you have to contact the admin (Noah/Maximilian), so that they can add you. Enjoy your coffee and see you soon."
        )
        user = User(
            first_name=None,
            surname=None,
            phonenumber=None,
            mailadress=None,
            rfid_id=data["rfid"],
        )
        self.sql_handler.add(user)
        self.sql_handler.commit()

        return {"user": user}

    def next(self, data):
        return CoffeeMachine.checkout_coffee


class Checkout_coffee(State):
    def __init__(self, display, sql):
        self.disp = display
        self.sql_handler = sql

    def run(self, data):
        consumption = Consumption(user_id=data["user"].id, product_id=1)
        self.sql_handler.add(consumption)
        self.sql_handler.commit()

        consumption = (
            self.sql_handler.query(Consumption)
            .join(User)
            .join(Product)
            .filter(User.rfid_id == data["user"].rfid_id)
            .all()
        )

        credit = (
            self.sql_handler.query(Credit)
            # .join(User)
            .filter(User.rfid_id == data["user"].rfid_id)
            .all()
        )

        total = 0
        for c in consumption:
            total -= c.product.price
        for c in credit:
            total += c.balance

        if total < 0:
            # remind people to add credit
            self.disp.show(
                f"Your current balance is {total:.2f}€.\n\n"
                "!!-----------------!!\n"
                "! Please add credit !\n"
                "!!-----------------!!\n"
            )
        else:
            self.disp.show(
                "Enjoy your coffee!\n" + f"Your current balance is {total:.2f}€."
            )

        # sleep(3)

    def next(self, data):
        return CoffeeMachine.wait


class Error(State):
    def __init__(self, display):
        self.disp = display

    def run(self, data):
        date = now()
        self.disp.show(f"An Error occured.\nContact the admins.\n{date}")

    def next(self, data):
        return CoffeeMachine.error


class Closed(State):
    def __init__(self, display):
        self.disp = display

    def run(self, data):
        date = now()
        self.disp.show(f"Interrupted.\nCoffeePy is offline.\n{date}")

    def next(self, data):
        return CoffeeMachine.closed


def main():
    disp = display()
    rfid = scanner()
    sql = SQL_Handler()

    CoffeeMachine.wait = Wait(disp)
    CoffeeMachine.identify_user = IdentifyUser(rfid, sql)
    CoffeeMachine.welcome_new_user = Welcome_new_user(disp, sql)
    CoffeeMachine.welcome_not_authenticated = Welcome_not_authenticated(disp)
    CoffeeMachine.welcome_existing_user = Welcome_existing_user(disp)
    CoffeeMachine.checkout_coffee = Checkout_coffee(disp, sql)
    CoffeeMachine.error = Error(disp)
    CoffeeMachine.closed = Closed(disp)

    machine = CoffeeMachine()

    try:
        while True:
            machine.run()
    except Exception as e:
        machine.error.run(None)
        raise e
    except KeyboardInterrupt:
        machine.closed.run(None)


if __name__ == "__main__":
    main()
