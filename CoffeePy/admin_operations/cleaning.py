from CoffeePy.database.CoffeeSQL import Cleaning, Consumption, User


class Cleaning:
    def __init__(self, sql):
        """
        Parameters:
            mode(str): User or general based cleaning plan
        """

        self.sql = sql

    def clean(self, user_id = None):
        self.user_id = user_id
        pass

    def last_cleaning(self)
        query = (
                self.sql_handler.query(Cleaning)
                .order_by(Cleaning.id.desc())
                )

        if self.user_id is not None:
            query = query.filter(User.id == self.user_id)

        return query.first()

    def consumption_since_cleaning(self):
        query = (
                self.sql_handler.query(Consumption)
                .order_by(Consumption.id.desc())
                )

        if self.user_id is not None:
            query = query.filter(User.id == self.user_id)

        return len(query.all())

class CleaningUniform(Cleaning):
    def __init__(self, sql, interval, cleaning_program):
        super().__init__(sql)
        mask = np.argsort(intervall)
        self.interval = interval[mask]
        self.cleaning_program = cleaning_program[mask]

    def clean(self, user_id=None):
        self.user_id = user_id
        last_cleaning = last_cleaning()
        consumptions = consumption_since_cleaning()
        for i, inter in enumerate(self.interval):
            if ( consumptions % inter == 0 ):
                        return self.cleaning_program[i]
        return False
