import click
from CoffeePy.database.CoffeeSQL import (
    SQL_Handler,
    User,
    Consumption,
    Product,
    Credit,
    Cleaning,
)
from CoffeePy.statemachine.CoffeeStatemachine import main as main_statemachine
from sqlalchemy import desc

__all__ = [
    "UserManagement",
    "CreditManagement",
    "ProductManagement",
    "ConsumptionManagement",
]

IGNORE_COLUMNS = ["id", "creation_date"]


class Management:
    sql = SQL_Handler()

    @classmethod
    def show(self):
        query = self.sql.query(self.table)
        for result in query:
            print(result)

    @classmethod
    def search(self):
        columns = self.table.__table__.columns.keys()

        query = "self.sql.query(self.table)"

        for col in columns:
            val = input(f"{col}: ") or None

            if val is not None:
                query += f".filter(self.table.{col}.ilike('%{val}%'))"

            if col == "id" and val is not None:
                # assuming we know the ID, we do not need to 'search'
                break

        results = eval(query + ".all()")

        return results

    @classmethod
    def add(self):
        columns = self.table.__table__.columns.keys()

        attributes = {}
        for col in columns:
            if col in IGNORE_COLUMNS:
                continue

            val = input(f"{col}: ") or None
            attributes[col] = val

        new = self.table(**attributes)
        query = self.sql.query(self.table).filter(self.table == new)

        if query:
            question = f"Add {new}? [y]/n "
            ans = (input(question) or "y") == "y"
            if ans:
                self.sql.add(new)
                self.sql.commit()

    @classmethod
    def delete(self):
        rows = self.search()

        for row in rows:
            question = f"Delete {row}? y/[n] "
            ans = (input(question) or "n") == "y"
            if ans:
                self.sql.delete(row)
                self.sql.commit()

    @classmethod
    def change(self):
        changed = False

        columns = self.table.__table__.columns.keys()

        rows = self.search()

        for row in rows:
            if len(rows) == 1:  # default answer of one result is yes
                ans = (input(f"Modify {row}? [y]/n ") or "y") == "y"
            else:
                ans = (input(f"Modify {row}? y/[n] ") or "n") == "y"

            if not ans:
                continue

            for col in columns:
                if col in IGNORE_COLUMNS:
                    continue
                new_value = input(f"{col} ({row.__getattribute__(col)}): ")
                if new_value != "":
                    row.__setattr__(col, new_value)
                    changed = True

            if not changed:
                print("Nothing to do.")

            else:
                question = f"Change to: {row}? [y]/n "
                ans = (input(question) or "y") == "y"

                if ans:
                    self.sql.add(row)
                    self.sql.commit()


class UserManagement(Management):
    table = User

    @classmethod
    def last_added(self):
        query = (
            self.sql.query(self.table)
            .order_by(desc(self.table.creation_date))
            .limit(3)
            .all()
        )
        for i, user in enumerate(query):
            print(f"{i}: {user}")


class ProductManagement(Management):
    table = Product


class ConsumptionManagement(Management):
    table = Consumption


class CreditManagement(Management):
    table = Credit


class CleaningManagement(Management):
    table = Cleaning


@click.group()
@click.pass_context
def main(context):
    """Management of coffeepy."""


@main.command()
def init():
    """Initialize a database."""


@main.command()
def start():
    """Start CoffeePy statemachine."""
    main_statemachine()


@main.group()
def add():
    """Group for add commands."""


@add.command()
def user():
    """Add user to sql database."""
    UserManagement.add()


@add.command()
def product():
    """Add product to sql database."""
    ProductManagement.add()


@add.command()
def consumption():
    """Add consumption to sql database."""
    ConsumptionManagement.add()


@add.command()
def credit():
    """Add credit to sql database."""
    CreditManagement.add()


@add.command()
def cleaning():
    """Add cleaning to sql database."""
    CleaningManagement.add()


@main.group()
def change():
    """Group for change commands."""


@change.command()
def user():
    """Change user in sql database."""
    UserManagement.change()


@change.command()
def product():
    """Change product in sql database."""
    ProductManagement.change()


@change.command()
def consumption():
    """Change consumption in sql database."""
    ConsumptionManagement.change()


@change.command()
def credit():
    """Change credit in sql database."""
    CreditManagement.change()


@change.command()
def cleaning():
    """Change cleaning in sql database."""
    CleaningManagement.change()


@main.group()
def show():
    """Group for show commands."""


@show.command()
@click.option(
    "--last-added", "-l", is_flag=True, default=False, help="Show last added user"
)
def user(last_added):
    """Show user."""
    if last_added:
        UserManagement.last_added()
    else:
        UserManagement.show()


@show.command()
def product():
    """Show product."""
    ProductManagement.show()


@show.command()
def consumption():
    """Show consumption."""
    ConsumptionManagement.show()


@show.command()
def credit():
    """Show credit."""
    CreditManagement.show()


@show.command()
def cleaning():
    """Show cleaning."""
    CleaningManagement.show()


if __name__ == "__main__":
    main()
