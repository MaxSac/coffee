import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    Float,
    DateTime,
    func,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from CoffeePy.locations import database
from CoffeePy.logging.CoffeeLogger import CoffeeLogger

module_logger = logging.getLogger("CoffeePi.coffeeSQL_module")

Base = declarative_base()


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    first_name = Column(String(32))
    surname = Column(String(32))
    phonenumber = Column(String(16))
    mailadress = Column(String(48))
    rfid_id = Column(String(16), nullable=False)
    creation_date = Column(DateTime, default=func.now())

    credit = relationship("Credit", back_populates="user")
    consumption = relationship("Consumption", back_populates="user")
    cleaning = relationship("Cleaning", back_populates="user")

    def __repr__(self):
        string = (
            "< User\n"
            + f"   id          = '{self.id}'\n"
            + f"   first_name  = '{self.first_name}'\n"
            + f"   surname     = '{self.surname}'\n"
            + f"   phonenumber = '{self.phonenumber}'\n"
            + f"   mailadress  = '{self.mailadress}'\n"
            + f"   rfid_id     = '{self.rfid_id}'\n"
            + f"   created     = '{self.creation_date}'\n"
            + ">"
        )
        return string


class Credit(Base):
    __tablename__ = "credit"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    balance = Column(Float)
    date = Column(DateTime, default=func.now())

    user = relationship("User", back_populates="credit")

    def __repr__(self):
        string = f"<Credit(id='{self.id}, user_id='{self.user_id}', balance='{self.balance}', date='{self.date}')>"
        return string


class Cleaning(Base):
    __tablename__ = "cleaning"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    programm = Column(String(16))
    date = Column(DateTime, default=func.now())

    user = relationship("User", back_populates="cleaning")

    def __repr__(self):
        string = f"<Cleaning(id='{self.id}, user_id='{self.user_id}', programm='{self.programm}', date='{self.date}')>"
        return string


class Product(Base):
    __tablename__ = "product"
    id = Column(Integer, primary_key=True)
    name = Column(String(16))
    price = Column(Float)

    consumption = relationship("Consumption", back_populates="product")

    def __repr__(self):
        string = f"<Product(id='{self.id}', name='{self.name}', price='{self.price}€')>"
        return string


class Consumption(Base):
    __tablename__ = "consumption"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    product_id = Column(Integer, ForeignKey("product.id"))

    user = relationship("User", back_populates="consumption")
    product = relationship("Product", back_populates="consumption")

    def __repr__(self):
        string = f"<Consumption(id='{self.id}', user_id='{self.user_id}', product_id='{self.product_id}')>"
        return string


class SQL_Handler:
    def __init__(self, db_file=database):
        db_path = f"sqlite:///{db_file}"
        self.engine = create_engine(db_path, echo=False)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)()

    def add(self, *args, **kwargs):
        self.session.add(*args, **kwargs)

    def commit(self, *args, **kwargs):
        self.session.commit(*args, **kwargs)

    def query(self, *args, **kwargs):
        return self.session.query(*args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.session.delete(*args, **kwargs)
