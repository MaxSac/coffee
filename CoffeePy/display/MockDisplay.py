from time import sleep
from subprocess import Popen


class MockDisplay:
    def __init__(self):
        pass

    def set_image(self, image):
        self.image = image

    def show(self):
        fn = "/tmp/coffeepy.display.png"
        self.image.save(fn)
        disp = Popen(["feh", fn])
        sleep(3)
        disp.kill()
