from PIL import ImageFont

font = ImageFont.truetype(
    font="/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf",
    size=30, index=0, encoding="",
)


def reflow(quote, width=400, font=font):
    words = quote.split(" ")
    reflowed = ''
    line_length = 0

    for i in range(len(words)):
        word = words[i] + " "
        word_length = font.getsize(word)[0]
        line_length += word_length

        if line_length < width:
            reflowed += word
        else:
            line_length = word_length
            reflowed = reflowed[:-1] + "\n" + word

        reflowed = reflowed.rstrip() + ' '

    return reflowed

def reflow_quote(quote, width=400, font=font):
    lines = quote.split("\n")
    print(lines)
    reflowed = ""
    for line in lines:
        reflowed += reflow(line, width, font).lstrip() + "\n"

    return reflowed.rstrip()

if __name__ == "__main__":
    flowing = reflow_quote("Hallo neuer Benutzer mit Namen.\n Neue Zeile.", 400)
    print(flowing)
