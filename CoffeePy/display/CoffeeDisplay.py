import logging

module_logger = logging.getLogger("CoffeePi.CoffeeDisplay")

try:
    from inky import InkyWHAT

    RASPBERRY_PI = True

except (ModuleNotFoundError, RuntimeError):
    module_logger.error("Could not import GPIO packages.")
    module_logger.warning("Assume not running on Raspberry Pi. Continue.")

    from CoffeePy.display.MockDisplay import MockDisplay

    RASPBERRY_PI = False


from PIL import Image, ImageFont, ImageDraw


class CoffeeDisplay:
    def __init__(self):
        if RASPBERRY_PI:
            self.display = InkyWHAT("black")
        else:
            self.display = MockDisplay()
        self.display_width = 400
        self.display_height = 300
        self.font = ImageFont.truetype(
            font="/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf",
            size=25,
            index=0,
            encoding="",
        )

    def draw(self, text):
        """Draw an image to display.

        Parameters
        ----------
        text : str
            Text to display.


        Returns
        -------
        img : ndarray
            Image to display.
        """
        empty = Image.new("RGB", (self.display_width, self.display_height))

        d = ImageDraw.Draw(empty)
        d.text((0, 0), text, font=self.font)

        # nur hinterfragen, wenn Zeit
        pal_img = Image.new("P", (1, 1))
        # weiss auf schwarz
        # pal_img.putpalette((255, 255, 255, 0, 0, 0, 255, 0, 0) + (0, 0, 0) * 252)
        # schwarz auf weiss
        pal_img.putpalette((0, 0, 0, 255, 255, 255, 255, 0, 0) + (0, 0, 0) * 252)

        img = empty.convert("RGB").quantize(palette=pal_img)

        return img

    def show(self, text):
        write = self.reflow_quote(text, self.display_width, self.font)
        image = self.draw(write)
        self.display.set_image(image)
        self.display.show()

    def show_image(self, image):
        img = image.crop((0, 0, self.display_width, self.display_height))
        self.display.set_image(img)
        self.display.show()

    def reflow_quote(self, quote, width, font):
        lines = quote.split("\n")
        reflowed = ""
        for line in lines:
            reflowed += self.reflow(line, width, font).lstrip() + "\n"

        return reflowed.rstrip()

    def reflow(self, quote, width, font):
        words = quote.split(" ")
        reflowed = ""
        line_length = 0

        for i in range(len(words)):
            word = words[i] + " "
            word_length = font.getsize(word)[0]
            line_length += word_length

            if line_length < width:
                reflowed += word
            else:
                line_length = word_length
                reflowed = reflowed[:-1] + "\n" + word

            reflowed = reflowed.rstrip() + " "

        return reflowed


def main():
    disp = CoffeeDisplay()
    disp.show("CoffeePy")


if __name__ == "__main__":
    main()
