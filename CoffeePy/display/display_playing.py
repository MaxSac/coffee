from inky import InkyWHAT
from PIL import Image, ImageDraw, ImageFont, ImageColor

def reflow_quote(self, quote, width, font):
    words = quote.split(" ")
    reflowed = ''
    line_length = 0

    for i in range(len(words)):
        word = words[i] + " "
        word_length = font.getsize(word)[0]
        line_length += word_length

        if line_length < width:
            reflowed += word
        else:
            line_length = word_length
            reflowed = reflowed[:-1] + "\n" + word

        reflowed = reflowed.rstrip() + ' '

    return reflowed


inkywhat = InkyWHAT("black")

txt = Image.new('RGB', (400,300))

monofont="/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf"
font = ImageFont.truetype(
        font=monofont,
        size=40,
        index=0,
        encoding=''
        )

d = ImageDraw.Draw(txt)

d.text((0,0), "0123456789abcdef", font=font, fill=(255,255,255))

pal_img = Image.new("P", (1, 1))
# pal_img.putpalette((255, 255, 255, 0, 0, 0, 255, 0, 0) + (0, 0, 0) * 252)
pal_img.putpalette((55, 55, 55, 255, 255, 255, 255, 0, 0) + (55, 55, 55) * 252)

img = txt.convert("RGB").quantize(palette=pal_img)

inkywhat.set_image(img)
inkywhat.show()
print('ready with showing')
