from qrcode import QRCode

PROTOCOL = "http"
HOST = "192.168.178.24"
PORT = ":5000"
ENDPOINT = "register"
URL = f"{PROTOCOL}://{HOST}{PORT}/{ENDPOINT}"


def create_qr_image(rfid: int | str):
    qr = QRCode(border=0)
    qr.add_data(f"{URL}/{rfid}")
    return qr.make_image(back_color="black", fill_color="white")
