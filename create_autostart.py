import os, pathlib, subprocess, logging, shutil
from CoffeePy.logging.CoffeeLogger import CoffeeLogger

logger = logging.getLogger("CoffeePi.create_autostart")

def main():
    logger.info("CoffeePy autostart will be created.")

    if os.geteuid() != 0:
        logger.debug("User is not authenticated, try to get root permissions.")
        ret = subprocess.run(
                f"sudo -v -p '[sudo] password for %u:'",
                shell=True,
                stderr=subprocess.STDOUT,
            )
        if ret.returncode == 1:
            logger.error("Authneticated has failed!")
            raise PermissionError(
                    "Without root privilegs systemd service for auto start can not be created."
                )

    dst = "/lib/systemd/system/coffeepi.service"
    src =pathlib.Path().absolute() / "coffeepi.service"
    if not pathlib.Path(dst).exists():
        logger.info(f"copy {src} → {dst}")
        shutil.copy2(src, dst)

    ret = subprocess.run(
            f"sudo systemctl enable coffeepi.service",
            shell=True,
            stderr=subprocess.STDOUT,
        )
    if ret.returncode == 0:
        logger.info("coffeepi.service successfully enabled.")


if __name__ == "__main__":
    log = CoffeeLogger(log_level="INFO")
    main()
